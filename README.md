#### Pipeline
The CI/CD pipeline automatically provisions application infrastructure with Terraform.  These infrastructure-as-code config files end with `.tf`. Once infrastructure is launched, the pipeline will also apply the Kubernetes manifests in the projet's root directory to the Amazon EKS cluster that is created as part of the infrastructure setup, deploying the Node.js application found in the partner repo to this one: https://gitlab.com/erm-projects/cloud-native-demo

#### Launching Infrastructure
To setup the infrastructure initially, trigger the CI/CD pipeline by pushing a commit to it or manually triggering the pipeline with the Gitlab GUI.  If you haven't made any changes to the infrastructure, the pipeline will still run but no changes will be applied.  This is made possible due to remote Terraform state storage with an S3 bucket. 

#### Deploying Application
Once the infrastructure has been provisioned, we use the `k8s-apply` job to deploy the Node.js application.  This is done by applying the `node-api-deployment.yaml` manifest to configure a Kubernetes "Deployment" (docs: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/), and then creating a subsequent LoadBalancer service (docs: https://kubernetes.io/docs/concepts/services-networking/service/) via the `node-api-service.yaml` manifest.  

#### Infrastructure Cleanup
To delete the infrastructure resources, manually trigger the `cleanup` job in the Gitlab pipeline.  Alternatively, follow the Step 7 below to delete infrastructure manually via the Terraform CLI. 

#### Manually Managing Infrastruture
    1. Clone the project
    2. cd into project directory
    3. `terraform init` to install dependencies
    4. `terraform plan -out tfplan.out` to validate and dry run the infrastructure changes, saving the output to a file for later application.  The name of this file is purely preference.
    5. `terraform apply tfplan.out` to apply the new infrastructure changes
    6. `terraform destroy` to tear down all infra setup with terraform