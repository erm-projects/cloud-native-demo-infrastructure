terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }

  backend "s3" {
    bucket = "cloud-native-demo"
    key    = "tfstate"
    region = "us-east-1"
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = var.region
}

resource "aws_eks_cluster" "cloud-native-demo" {
  name     = "cloud-native-demo"
  role_arn = aws_iam_role.cloud-native-demo.arn

  vpc_config {
    subnet_ids = ["subnet-0ad45d72d701055b8", "subnet-0d9d1b5da085475d4"]
  }

  depends_on = [aws_iam_role_policy_attachment.cloud-native-demo-AmazonEKSClusterPolicy,
  aws_iam_role_policy_attachment.cloud-native-demo-AmazonEKSVPCResourceController]
}

module "eks_managed_node_group" {
  source       = "terraform-aws-modules/eks/aws//modules/eks-managed-node-group"
  name         = "cnd-node-group"
  cluster_name = aws_eks_cluster.cloud-native-demo.name

  subnet_ids = aws_eks_cluster.cloud-native-demo.vpc_config[0].subnet_ids

  min_size     = 2
  max_size     = 10
  desired_size = 2

  instance_types = ["t2.large"]
  capacity_type  = "SPOT"

}
