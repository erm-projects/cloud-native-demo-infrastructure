variable "region" {
  description = "The AWS region to use"
  type        = string
  default     = "us-east-1"
}

variable "cluster_name" {
  default = "cloud-native-demo"
  type    = string
}

variable "cidr_block" {
  description = "The CIDR block to describe the allowed inbound IP ranges."
  default     = "10.0.1.0/24"
}

variable "cluster_security_group_id" {
  description = "Security group ids attached to the cluster control plane"
  type        = string
  default     = "vpc-06ceacf64b271a31f"
}