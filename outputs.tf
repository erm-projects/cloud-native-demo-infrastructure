output "cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = var.cluster_name
}

output "cluster_endpoint" {
  description = "Endpoint for EKS control plane"
  value       = aws_eks_cluster.cloud-native-demo.endpoint
}

output "region" {
  description = "AWS region"
  value       = var.region
}
